<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Storage;
use Intervention\Image\ImageManager;
use Image;
use App\Model\Images;
use Uuid;
use Log;
use Validator;
use Session;

class GalleroController extends Controller {

    public function index($size = 'medium') {

        $data = Images::all();

        return view('gallero.content.'.$size, ['data' => $data, 'size' => $size]);
    }

    public function uploads(request $request) {

        $input = $request->all();

        $input_data = $request->all();

        $validator = Validator::make(
        $input_data, [
        'filename.*' => 'mimes:jpg,jpeg,png,bmp|max:2000'
        ], [
        'filename.*.mimes' => 'Only jpeg,png and bmp images are allowed',
        'filename.*.max' => 'Sorry! Maximum allowed size for an image is 2MB',
        ]
        );

        if ($validator->fails()) {

            return view('gallero.content.failed', ['data' => $validator->errors()]);
        }


        if ($files = $request->file('filename')) {

            foreach ($files as $file) {

                $extension = $file->getClientOriginalExtension();

                $file_name = Uuid::generate() . '.' . $extension;

                Storage::disk('s3')->put($file_name, file_get_contents($file));

                // Set small
                $this->resize($file, $file_name, 'small', 140);

                // Set medium
                $this->resize($file, $file_name, 'medium', 320);

                // Set large
                $this->resize($file, $file_name, 'large', 500);

                // Storing into database
                $data = new Images;
                $data->image_id = Uuid::generate();
                $data->image_name = $file_name;
                $data->image_ext = $extension;

                // Save data
                $data->save();
            }
        }

        Session::set('success', 'Updated Success');
        return redirect('/');
    }

    private function resize($file, $name, $prefix, $width) {

        $img = Image::make($file)->resize($width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg');

        Storage::disk('s3')->put($prefix . '_' . $name, $img->__toString());
    }

    public function image($filename = '') {
        return getImage($filename);
    }

    public function failed() {
        return view('gallero.content.failed');
    }

}
