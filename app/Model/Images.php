<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Images extends Model {
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * Set custom name for primary key.
     *
     * @var string
     */
    protected $primaryKey = 'image_id';

    /**
     * Set incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
}
