<?php

function getImage($imagePath) {
    
    if (Storage::disk('s3')->exists($imagePath)) {
        
        $file =  Storage::disk('s3')->get($imagePath);
        
        return Image::make($file)->response();
        
    } else {
        
        return 'No Image';
        
    }
    
}

?>