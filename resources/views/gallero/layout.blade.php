<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gallero</title>

        @include('gallero.style')

    </head>
    <body>

        @include('gallero.navigator')

       

        @yield('content')

        @include('gallero.script')

        @yield('script-navigator')
        @yield('script')

    </body>
</html>