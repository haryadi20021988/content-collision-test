@extends('gallero.layout')

@section('content')

<div class="container-fluid container-custome image-container" >


    @include('gallero.alert')

    <?php foreach ($data as $key => $item): ?>

        <?php if ($key == 0 || $key % 3 == 0): ?>
            <div class="row">
            <?php endif; ?>

            <div class="col-md-4">
                <div class="card card-image"  >
                    <img class="card-img-top" src="<?= url('image/'.$size . '_' . $item->image_name); ?>" >
                </div>
            </div>

            <?php if (($key + 1) % 3 == 0 || count($data) == ($key + 1)): ?>
            </div>
        <?php endif; ?>
        <br/>

    <?php endforeach; ?>


</div>

@stop

