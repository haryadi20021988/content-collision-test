@extends('gallero.layout')

@section('content')

<div class="container-fluid container-custome image-container" >

    <div class="alert alert-danger" role="alert">
        <ul>
            <?php foreach ($data->all() as $key => $item): ?>

                <li><?= $item; ?></li>

            <?php endforeach; ?>
        </ul>
        
        <a href="<?= url('/') ?>" class="btn btn-info" >Back To Image List</a>
    </div>


</div>

@stop

