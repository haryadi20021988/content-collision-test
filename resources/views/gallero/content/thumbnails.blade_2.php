@extends('gallero.layout')



@section('content')


<style type="text/css">

    body{
        background-color:#F1F1F1;
    }
    .Collage img{
        opacity:0;
        box-shadow:0px 3px 4px #dcdcdc;
        border-radius: 6px;
        /* 
        * Change this to try different borders
        */
        border:6px solid #FFF;
    }
    .Collage{
        /* 
         * Change this to set the spacing of the images in the grid
        */
        padding:10px;
    }

    .footnote{
        font-family:arial;
        color:#999;
        padding:10px;
        font-size:12px;
    }
    .footnote a{
        color:#09f;
        text-decoration:none;
    }


</style>

<div class="container-fluid center" >

    <div class="Collage effect-parent">
        <?php foreach ($data as $key => $item): ?>


            <img  src="<?= asset('upload/' . $size . '_' . $item->image_name); ?>" >


        <?php endforeach; ?>
    </div>

</div>

@stop

@section('script')

<script type="text/javascript">
    
     
        var file = $(this)[0].files[0];
        
        var formData = new FormData();
         
        formData.append("filename[]", file);
        
        $.ajax({
            type:'POST',
            url: $('#uploadimage').attr('action'),
            data:formData,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success");
                console.log(data);
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });

    

</script>

@stop