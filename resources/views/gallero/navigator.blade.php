<nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top" style="background-color: #e3f2fd;">

    <a class="navbar-brand" href="#">
        <h4>
            <i class="oi oi-camera-slr"></i>
            Gallero
        </h4>
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= url('large'); ?>">
                    <i class="oi oi-grid-two-up" ></i> 
                    Large
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= url('medium'); ?>">
                    <i class="oi oi-grid-three-up" ></i> 
                    Medium
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= url('small'); ?>">
                    <i class="oi oi-grid-four-up" ></i> 
                    Small
                </a>
            </li>
        </ul>
        <form  id="uploadimage" action="<?= url('uploads'); ?>" method="post" enctype="multipart/form-data" class="form-inline my-2 my-lg-0" >

            <input type="file" name="filename[]"  class="" style="display: none;"  multiple="multiple" />

           
            &nbsp;
            <button class="btn btn-info btn-upload-file" type="button">
                <i class="oi oi-cloud-upload"></i>
                Upload 
            </button>

        </form>

    </div>
</nav>

@section('script-navigator')

<script type="text/javascript">
    
    // Trigger file input 
    $('body').on('click' ,'.btn-upload-file', function(){
        $('input[name="filename[]"]').trigger('click');
    });
    
    $('body').on('change','input[name="filename[]"]', function(){
        
        $('#uploadimage').submit();
       
    
    });
    
</script>

@stop