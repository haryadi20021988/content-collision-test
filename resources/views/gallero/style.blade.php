<link rel="stylesheet" href="<?= url('assets/bootstraps/css/bootstrap.min.css'); ?>" >
<link rel="stylesheet" href="<?= url('assets/open-iconic/font/css/open-iconic-bootstrap.min.css'); ?>" >
<link rel="stylesheet" href="<?= url('assets/justified/jquery.justified.css'); ?>" >
<link rel="stylesheet" href="<?= url('assets/collageplus/css/transitions.css'); ?>" >

<style type="text/css" >

    .container-custome{
        margin-top: 20px;
    }

    .card-image :hover{
        cursor: pointer;
    }

    .navbar-fixed-top { 
        position: sticky !important; 
        top: 0px; 
        width: 100%;
        z-index: 1000;
    }

</style>